package com.webservices.swmap.service;

import com.webservices.swmap.enums.ERole;
import com.webservices.swmap.model.Role;
import org.springframework.validation.annotation.Validated;

import java.util.Optional;

@Validated
public interface RoleService {

	/**
	 * Retourne un {@link Role} s'il existe dans la base.
	 *
	 * @param name le nom du rôle à retrouver
	 *
	 * @return la ligne correspondante au nom fourni, si elle existe, une exception sinon.
	 */
	Optional<Role> getByName(ERole name);
}
