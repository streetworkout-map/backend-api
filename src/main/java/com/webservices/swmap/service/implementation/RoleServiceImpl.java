package com.webservices.swmap.service.implementation;

import com.webservices.swmap.enums.ERole;
import com.webservices.swmap.model.Role;
import com.webservices.swmap.repository.RoleRepository;
import com.webservices.swmap.service.RoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

	private final RoleRepository repository;

	/**
	 * Constructeur avec paramètre (injection des dépendances).
	 *
	 * @param repository Le repository de persistence des {@link Role}
	 */
	public RoleServiceImpl(RoleRepository repository) {
		this.repository = repository;
	}

	@Override
	public Optional<Role> getByName(ERole name) {
		return repository.getByName(name);
	}
}
