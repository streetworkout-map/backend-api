package com.webservices.swmap.service.implementation;

import com.webservices.swmap.model.Equipment;
import com.webservices.swmap.model.Park;
import com.webservices.swmap.model.Verifier;
import com.webservices.swmap.repository.EquipmentRepository;
import com.webservices.swmap.repository.ParkRepository;
import com.webservices.swmap.repository.VerifierRepository;
import com.webservices.swmap.service.ParkService;
import com.webservices.swmap.util.Util;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

/**
 * Implémentation de l'interface {@link Park}. Partie métier de l'application, c'est ici que vont se retrouver les traitements sur les données.
 */
@Service
@Transactional
public class ParkServiceImpl implements ParkService {

	private final ParkRepository repository;
	private final EquipmentRepository equipmentRepository;

	private final VerifierRepository verifierRepository;

	/**
	 * Constructeur avec paramètre(s) (injection des dépendances)
	 *
	 * @param repository Le repository de persistence des {@link Park}
	 * @param equipmentRepository Le repository de persistence des {@link Equipment}
	 * @param verifierRepository Le repository de persistence des {@link Verifier}
	 */
	@Inject
	public ParkServiceImpl(ParkRepository repository, EquipmentRepository equipmentRepository, VerifierRepository verifierRepository) {
		this.repository = repository;
		this.equipmentRepository = equipmentRepository;
		this.verifierRepository = verifierRepository;
	}

	@Override
	public Park createPark(Park park) {
		Equipment equipment = equipmentRepository.createEquipment(park.getEquipment());
		if (equipment == null) return null;

		park.setEquipment(equipment);
		Park result = repository.createPark(park);
		//Si la création d'un Equipment réussi mais que celle d'un Park échoue, on risque d'avoir en base un
		// equipment_id lié à aucun Park.
		if (result == null) equipmentRepository.deleteEquipment(equipment.getEquipmentId());
		return result;
	}

	@Override
	public Park updatePark(Park park) {
		equipmentRepository.updateEquipment(park.getEquipment());
		return repository.updatePark(park);
	}

	@Override
	public Park addParkVerifier(Verifier verifier) {
		//Récupération du park s'assurer de l'existence du parkId
		Park park = repository.getParkById(verifier.getParkId());

		if (park == null) return null;

		//Ajout du verifier au park
		Verifier createdVerifier = verifierRepository.createVerifier(verifier);

		if (createdVerifier == null) return null;

		return park;
	}

	@Override
	public boolean deletePark(Long parkId, Long equipmentId) {
		return (repository.deletePark(parkId) && equipmentRepository.deleteEquipment(equipmentId));
	}

	@Override
	public Park removeParkVerifier(Verifier verifier) {
		//Récupération du park pour s'assurer de l'existence du parkId
		Park park = repository.getParkById(verifier.getParkId());

		if (park == null) return null;

		//Suppression du Verifier
		if (! verifierRepository.deleteVerifier(verifier)) return null;

		return park;
	}

	@Override
	public List<Park> getParks() {
		List<Park> listPark = repository.getParks();
		for (Park park : listPark) {
			List<Long> usersId = repository.getUsersVerified(park.getParkId());
			park.setListVerifier(usersId);
		}
		return listPark;
	}

	@Override
	public Park getParkById(Long parkId) {
		Park park = repository.getParkById(parkId);

		List<Long> usersId = repository.getUsersVerified(parkId);
		park.setListVerifier(usersId);

		return park;
	}

	@Override
	public List<Park> getParkByLocation(Double latitude, Double longitude, Double distance) {
		List<Park> result = repository.getParks();
		long roundedDistance = Math.round(distance);

		//Variable qui va nous servir d'index
		int i = 0;
		for (int j = 0; j < result.size(); j++) {
			if (Util.distanceBetweenCoordinates(latitude, longitude, result.get(i)
			                                                               .getLatitude()
			                                                               .doubleValue(), result.get(i)
			                                                                                     .getLongitude()
			                                                                                     .doubleValue()) > roundedDistance) {
				result.remove(i);
				//Lorsqu'on supprime une ligne du tableau, ses index sont remis à jour. Comme on a une ligne de
				// moins, il faut que notre index soit, lui aussi, mis à jour.
				i--;
			} else {
				List<Long> usersId = repository.getUsersVerified(result.get(i).getParkId());
				result.get(i).setListVerifier(usersId);
			}
			i++;
		}

		return result;
	}
}
