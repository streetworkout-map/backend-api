package com.webservices.swmap.model;

import com.webservices.swmap.enums.ERole;

/**
 * Modèle métier {@link Role}.
 */
public class Role {

	private Long roleId;
	private ERole name;

	/**
	 * Constructeur vide pour la désérialisation
	 */
	public Role() {
	}

	/**
	 * Constructeur via le Builder
	 *
	 * @param builder builder
	 */
	private Role(Builder builder) {
		setRoleId(builder.roleId);
		setName(builder.name);
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public ERole getName() {
		return name;
	}

	public void setName(ERole name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Role{" +
				"roleId=" + roleId +
				", name=" + name +
				'}';
	}

	/**
	 * {@code Role} builder static inner class.
	 */
	public static final class Builder {
		private Long roleId;
		private ERole name;

		public Builder() {
		}

		public Builder(Role copy) {
			this.roleId = copy.getRoleId();
			this.name = copy.getName();
		}

		/**
		 * Sets the {@code roleId} and returns a reference to this Builder enabling method chaining.
		 *
		 * @param val the {@code roleId} to set
		 * @return a reference to this Builder
		 */
		public Builder roleId(Long val) {
			roleId = val;
			return this;
		}

		/**
		 * Sets the {@code name} and returns a reference to this Builder enabling method chaining.
		 *
		 * @param val the {@code name} to set
		 * @return a reference to this Builder
		 */
		public Builder name(ERole val) {
			name = val;
			return this;
		}

		/**
		 * Returns a {@code Role} built from the parameters previously set.
		 *
		 * @return a {@code Role} built with parameters of this {@code Role.Builder}
		 */
		public Role build() {
			return new Role(this);
		}
	}
}
