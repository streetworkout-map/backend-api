package com.webservices.swmap.repository.implementation;

import com.webservices.swmap.enums.ERole;
import com.webservices.swmap.model.Role;
import com.webservices.swmap.repository.RoleRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.sql.ResultSet;
import java.util.Optional;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class RoleRepositoryImpl implements RoleRepository {

	/** nom de colonne et de paramètre pour l'attribut role_id */
	public static final String COL_ROLE_ID = "role_id";

	/** nom de colonne et de paramètre pour l'attribut name */
	public static final String COL_NAME = "name";

	/** Logger de la classe */
	private static final Logger LOGGER = LogManager.getLogger();

	/**
	 * Le jdbc modèle à utiliser pour l'exécution des requêtes SQL
	 */
	private final NamedParameterJdbcTemplate jdbcTemplate;

	/**
	 * Récupération de la requête MySQL de sélection d'un utilisateur.
	 */
	@Value("${swmap.request.role.getRoleByName}")
	private String getRoleByNameMySql;

	/**
	 * Constructeur avec paramètre(s).
	 *
	 * @param jdbcTemplate le modèle à utiliser
	 */
	@Inject
	public RoleRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public Optional<Role> getByName(ERole name) {
		SqlParameterSource params = new MapSqlParameterSource().addValue(COL_NAME, name.toString());

		RowMapper<Role> roleRowMapper = (ResultSet rs, int rowNumber) -> new Role.Builder()
				.roleId(rs.getLong(COL_ROLE_ID))
				.name(ERole.valueOf(rs.getString(COL_NAME)))
				.build();

		Role role = null;
		String messageInfo = "Recuperation du role [" + name + "]. ";
		String messageError = "Impossible d'acceder au role [" + name + "].";

		try {
			role = jdbcTemplate.queryForObject(getRoleByNameMySql, params, roleRowMapper);
			LOGGER.info(messageInfo, role);
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error(messageError);
		}

		return Optional.of(role);
	}
}
