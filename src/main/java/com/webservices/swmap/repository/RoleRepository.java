package com.webservices.swmap.repository;

import com.webservices.swmap.enums.ERole;
import com.webservices.swmap.model.Role;

import java.util.Optional;

/**
 * Interface d'accès au Repository de {@link Role}.
 */
public interface RoleRepository {

	/**
	 * Retourne un {@link Role} s'il existe dans la base.
	 *
	 * @param name le nom du rôle à retrouver
	 *
	 * @return la ligne correspondante au nom fourni, si elle existe, une exception sinon.
	 */
	Optional<Role> getByName(ERole name);
}
