package com.webservices.swmap.controller;

import com.webservices.swmap.enums.ERole;
import com.webservices.swmap.model.Role;
import com.webservices.swmap.model.User;
import com.webservices.swmap.payload.request.LoginRequest;
import com.webservices.swmap.payload.request.SignupRequest;
import com.webservices.swmap.payload.response.JwtResponse;
import com.webservices.swmap.payload.response.MessageResponse;
import com.webservices.swmap.security.jwt.JwtUtils;
import com.webservices.swmap.security.services.UserDetailsImpl;
import com.webservices.swmap.service.RoleService;
import com.webservices.swmap.service.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@CrossOrigin
@RestController
@RequestMapping(path = AuthRestController.ROOT_PATH)
public class AuthRestController {

	public static final String ROOT_AUTH = "/auth";
	public static final String ROOT_PATH = "${url.rest.controller.mapping.public}" + ROOT_AUTH;
	private static final String PATH_SIGNUP = "/signup";
	private static final String PATH_LOGIN = "/login";

	@Autowired
	AuthenticationManager authenticationManager;

	UserService service;

	@Autowired
	RoleService roleService;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	public AuthRestController(UserService service) {
		this.service = service;
	}

	@PostMapping(path = PATH_SIGNUP)
	public ResponseEntity signUp(@Valid @RequestBody SignupRequest request) {

		// Vérifie si le pseudo est déjà pris
		if (service.existsByUsername(request.getUsername())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Nom d'utilisateur est déjà utilisé!"));
		}

		// Vérifie si le mail est déjà pris
		if (service.existsByEmail(request.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Adresse mail est déjà utilisée!"));
		}

		// Crée un nouvel utilisateur
		User user = new User.Builder()
				.mail(request.getEmail())
				.username(request.getUsername())
				.password(encoder.encode(request.getPassword()))
				.build();

		Set<String> strRoles = request.getRole();
		Set<Role> roles = new HashSet<>();

		String roleNotFound = "Erreur: le rôle est introuvable.";

		// Si role non-fourni on initialise avec 'ROLE_USER'
		if (strRoles == null) {
			Role userRole = roleService.getByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException(roleNotFound));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
					case "ROLE_ADMIN" -> {
						Role adminRole = roleService.getByName(ERole.ROLE_ADMIN)
								.orElseThrow(() -> new RuntimeException(roleNotFound));
						roles.add(adminRole);
					}
					case "ROLE_MODERATOR" -> {
						Role modRole = roleService.getByName(ERole.ROLE_MODERATOR)
								.orElseThrow(() -> new RuntimeException(roleNotFound));
						roles.add(modRole);
					}
					default -> {
						Role userRole = roleService.getByName(ERole.ROLE_USER)
								.orElseThrow(() -> new RuntimeException(roleNotFound));
						roles.add(userRole);
					}
				}
			});
		}

		user.setUsername(request.getUsername());
		user.setMail(request.getEmail());
		user.setRoles(roles);
		user.setCreationAgent("auth-controller");

		service.createUser(user);

		return ResponseEntity.ok(new MessageResponse("L'utilisateur s'est bien enregistré !"));
	}

	@PostMapping(path = PATH_LOGIN)
	public ResponseEntity authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

		List<String> roles = userDetails.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.toList();

		return ResponseEntity.ok(new JwtResponse(jwt,
				userDetails.getId(),
				userDetails.getUsername(),
				userDetails.getEmail(),
				roles));
	}
}
