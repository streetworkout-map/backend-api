package com.webservices.swmap.payload.request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

import java.util.Set;

/**
 * Représente la requête d'enregistrement.
 */
public class SignupRequest {
	@Size(max=50, message="Le nom d'utilisateur ne doit pas dépasser plus que 50 caractères")
	@NotBlank
	private String username;

	@Size(max=100, message="Le mail ne doit pas dépasser plus que 100 caractères")
	@Email(message="Veuillez ajouter un mail valide", regexp=".+[@].+[\\.].+")
	@NotBlank
	private String email;

	private Set<String> role;

	@NotBlank
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<String> getRole() {
		return role;
	}

	public void setRole(Set<String> role) {
		this.role = role;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
