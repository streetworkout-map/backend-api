package com.webservices.swmap.security.jwt;

import com.webservices.swmap.security.services.UserDetailsImpl;
import io.jsonwebtoken.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Date;

import static io.jsonwebtoken.security.Keys.secretKeyFor;

/**
 * Classe utilitaire pour JWT Bearer
 */
@Component
public class JwtUtils {

	private static final Logger LOGGER = LogManager.getLogger(JwtUtils.class);

	private final Key jwtSecret = secretKeyFor(SignatureAlgorithm.HS256);

	@Value("${swmap.app.jwtExpirationMs}")
	private int jwtExpirationMs;

	public String generateJwtToken(Authentication authentication) {
		UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();

		LOGGER.info("Utilisateur connecté : {}", authentication.getName());

		return Jwts.builder()
				.setSubject((userPrincipal.getUsername()))
				.setIssuedAt(new Date())
				.setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
				.signWith(jwtSecret)
				.compact();
	}

	public boolean validateJwtToken(String authToken) {
		LOGGER.info("Utilisateur faisant l'appel : {}", getUserNameFromJwtToken(authToken));
		try {
			Jwts.parserBuilder()
					.setSigningKey(jwtSecret)
					.build()
					.parseClaimsJws(authToken);
			return true;
		} catch (MalformedJwtException e) {
			LOGGER.error("Invalid JWT token: {}", e.getMessage());
		} catch (ExpiredJwtException e) {
			LOGGER.error("JWT token is expired: {}", e.getMessage());
		} catch (UnsupportedJwtException e) {
			LOGGER.error("JWT token is unsupported: {}", e.getMessage());
		} catch (IllegalArgumentException e) {
			LOGGER.error("JWT claims string is empty: {}", e.getMessage());
		}

		return false;
	}

	public String getUserNameFromJwtToken(String token) {
		return Jwts.parserBuilder()
				.setSigningKey(jwtSecret)
				.build()
				.parseClaimsJws(token).getBody().getSubject();
	}
}
